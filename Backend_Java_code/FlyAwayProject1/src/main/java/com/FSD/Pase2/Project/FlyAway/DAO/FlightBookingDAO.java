package com.FSD.Pase2.Project.FlyAway.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.FSD.Phase2.Project.FlyAway.Entity.Airport;
import com.FSD.Phase2.Project.FlyAway.Entity.FlightBookings;
import com.FSD.Phase2.Project.FlyAway.SessionUtil.SessionUtil;

public class FlightBookingDAO {
	
	public void addFlightBookings(FlightBookings bean)
	{
		Session session = SessionUtil.getSession();        
	    Transaction tx = session.beginTransaction();
	    addFlightBookings(session,bean);        
        tx.commit();
        session.close();
        
    }
	
	public void  addFlightBookings(Session session, FlightBookings bean)
	{
		FlightBookings fb=new  FlightBookings();
		fb.setCustomernumber(bean.getCustomernumber());
		fb.setNumber(bean.getNumber());
		session.save(fb);
		
	}
   
	public List<FlightBookings> getFlightBookings()
	
	{
		Session session = SessionUtil.getSession();
		Query query=session.createQuery("from FlightBookings");
		List<FlightBookings> fbs=query.list();
		session.close();
		return fbs;
		
	}

	public FlightBookings getFlightBookingById(String number) {
		Session session = SessionUtil.getSession();
				  Query query = session.getSession().createQuery("from FlightBookings where number=:newnumber");
		  query.setParameter("newnumber",number);
		  return (FlightBookings) query.list().get(0);
	}
	
	
	public int updateFlightBookings(String id,FlightBookings bean)
	{
		Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
		String hql="update FlightBookings set customernumber=:new_customernumber where id = :id";
		Query query=session.createQuery(hql);
		query.setString("id",bean.getNumber());
        query.setString("new_customernumber",bean.getCustomernumber());
		int result=query.executeUpdate();
		System.out.println("number of records updated " +result);
		tx.commit();
        session.close();
		return result;
	}

	    public int deleteFlightBookings(int id) {
	        Session session = SessionUtil.getSession();
	        Transaction tx = session.beginTransaction();
	        String hql = "delete from FlightBookings where id = :id";
	        Query query = session.createQuery(hql);
	        query.setInteger("id",id);
	        int rowCount = query.executeUpdate();
	        System.out.println("Rows affected: " + rowCount);
	        tx.commit();
	        session.close();
	        return rowCount;
	    }

}
