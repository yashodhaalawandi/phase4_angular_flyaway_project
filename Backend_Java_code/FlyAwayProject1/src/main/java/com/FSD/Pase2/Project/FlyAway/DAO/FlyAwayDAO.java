package com.FSD.Pase2.Project.FlyAway.DAO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.FSD.Phase2.Project.FlyAway.Entity.Airport;
import com.FSD.Phase2.Project.FlyAway.Entity.Flight;
import com.FSD.Phase2.Project.FlyAway.Entity.Users;
import com.FSD.Phase2.Project.FlyAway.SessionUtil.SessionUtil;

public class FlyAwayDAO {

	public void addAirport(Airport bean) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		addAirport(session, bean);
		tx.commit();
		session.close();

	}

	public void addAirport(Session session, Airport bean) {
		Airport a = new Airport();
		a.setAirportcode(bean.getAirportcode());
		a.setAirportname(bean.getAirportname());
		a.setLocation(bean.getLocation());
		session.save(a);

	}

	public List<Airport> getAirport()

	{
		Session session = SessionUtil.getSession();
		Query query = session.createQuery("from Airport");
		List<Airport> lisair = query.list();
		session.close();
		return lisair;

	}

	  
	
	public int updateAirport(String id,Airport bean)
	{
		Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
		String hql="update Airport set airportname=:new_airportname where id = :id";
		Query query=session.createQuery(hql);
		query.setString("id",bean.getAirportcode());
        query.setString("new_airportname",bean.getAirportname());
		int result=query.executeUpdate();
		System.out.println("number of records updated " +result);
		tx.commit();
        session.close();
		return result;
	}
	 

	

	public Airport getAirportById(String airportcode) {
		Session session = SessionUtil.getSession();
		Query query = session.getSession().createQuery("from Airport where airportcode=:newairportcode");
		query.setParameter("newairportcode", airportcode);
		return (Airport) query.list().get(0);
	}

	public int deleteAirport(int id) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "delete from Airport where id = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}

}