package com.FSD.Pase2.Project.FlyAway.DAO;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.FSD.Phase2.Project.FlyAway.Entity.Users;
import com.FSD.Phase2.Project.FlyAway.SessionUtil.SessionUtil;

public class UsersDAO {

	private int count;

	public void addUsers(Users bean) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		addUsers(session, bean);
		tx.commit();
		session.close();

	}

	public void addUsers(Session session, Users bean) {
		Users usr = new Users();
		usr.setUsername(bean.getUsername());
		usr.setPassword(bean.getPassword());
		usr.setFirstName(bean.getFirstName());
		usr.setLastName(bean.getLastName());
		usr.setEmail(bean.getEmail());
		usr.setPhone(bean.getPhone());
		usr.setUsertype(bean.getUsertype());
		session.save(usr);

	}

	public List<Users> getUsers()

	{
		Session session = SessionUtil.getSession();
		Query query = session.createQuery("from Users");
		List<Users> usr = query.list();
		session.close();
		return usr;

	}

	public boolean getUsersbyEmailId(Users users) {
		Session session = SessionUtil.getSession();
		String hql = "select u.email from Users u where u.email=:Users_Email and u.password=:Users_password";
		Query query = session.createQuery(hql);
		query.setString("Users_Email", users.getEmail());
		query.setString("Users_password", users.getPassword());

		for (Iterator it = query.iterate(); it.hasNext();) {
			it.next();
			count++;
		}
		System.out.println("Total rows: " + count);
		if (count == 1) {
			return true;
		} else {
			return false;
		}

	}

	public Users getUserProfile(String email) {
		Session session = SessionUtil.getSession();
		Query query = session.getSession().createQuery("from Users where email = :email");
		query.setParameter("email", email);
		return (Users) query.list().get(0);
	}

	public int updateUsers(String id, Users bean) {

		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		System.out.println("before update" + bean);
		String hql = "update Users set password=:pwd where id = :id";
		Query query = session.createQuery(hql);
		query.setString("id", bean.getEmail());
		query.setString("pwd", bean.getPassword());
		int result = query.executeUpdate();
		System.out.println("number of records updated " + result);
		tx.commit();
		session.close();
		return result;
	}

	public int deleteUsers(String id) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "delete from Users where id = :id";
		Query query = session.createQuery(hql);
		query.setString("id", id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}

}
