package com.FSD.Pase2.Project.FlyAway.DAO;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.FSD.Phase2.Project.FlyAway.Entity.Passenger;
import com.FSD.Phase2.Project.FlyAway.SessionUtil.SessionUtil;

public class PassangerDAO {

	public void addPassengers(Passenger bean) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		addPassengers(session, bean);
		tx.commit();
		session.close();

	}

	public void addPassengers(Session session, Passenger passenger) {
		Passenger pass = new Passenger();
		pass.setAddress(passenger.getAddress());
		pass.setEmailId(passenger.getEmailId());
		pass.setName(passenger.getName());
		pass.setPassnumber(passenger.getPassnumber());
		session.save(pass);

	}

	public List<Passenger> getPassengers()

	{
		Session session = SessionUtil.getSession();
		Query query = session.createQuery("from Passenger");
		List<Passenger> passengers = query.list();
		session.close();
		return passengers;

	}

	public int updatePassenger(int id, Passenger pass) {

		if (id <= 0)
			return 0;
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "update Passenger set name = :Passanger_Name, Address=:Passenger_Address where id = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		query.setString("Passanger_Name", pass.getName());
		query.setString("Passenger_Address", pass.getAddress());
		int result = query.executeUpdate();
		System.out.println("number of records updated " + result);
		tx.commit();
		session.close();
		return result;
	}

	public int deletePassenger(int id) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "delete from Passenger where id = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}

}
