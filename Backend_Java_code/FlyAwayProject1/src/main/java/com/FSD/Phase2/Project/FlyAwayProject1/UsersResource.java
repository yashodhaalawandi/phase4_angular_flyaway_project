package com.FSD.Phase2.Project.FlyAwayProject1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.FSD.Pase2.Project.FlyAway.DAO.FlightBookingDAO;
import com.FSD.Pase2.Project.FlyAway.DAO.FlightDAO;
import com.FSD.Pase2.Project.FlyAway.DAO.FlyAwayDAO;
import com.FSD.Pase2.Project.FlyAway.DAO.UsersDAO;
import com.FSD.Phase2.Project.FlyAway.Entity.Flight;
import com.FSD.Phase2.Project.FlyAway.Entity.Users;

@Path("users")
public class UsersResource {

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsers() {
		// System.out.println("List of Users");
		UsersDAO udao = new UsersDAO();
		List<Users> users = udao.getUsers();
		return Response.ok(udao.getUsers(), MediaType.APPLICATION_JSON).build();
	}

	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addUsers(Users usr) {

		UsersDAO udao = new UsersDAO();
		udao.addUsers(usr);

		return Response.status(200).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Max-Age", "1209600").entity(usr).build();

	}

	@POST
	@Path("/signin")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(Users user) {
		UsersDAO dao = new UsersDAO();
		boolean status = dao.getUsersbyEmailId(user);
		Map<String, String> response = new HashMap<>();
		if (status) {
			response.put("status", "success");
			response.put("token", UUID.randomUUID().toString());

			return Response.status(200).header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
					.header("Access-Control-Allow-Credentials", "true")
					.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
					.header("Access-Control-Max-Age", "1209600").entity(response).build();
		} else {
			response.put("status", "failed");
			return Response.status(404).header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
					.header("Access-Control-Allow-Credentials", "true")
					.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
					.header("Access-Control-Max-Age", "1209600").entity(response).build();
		}

	}

	@GET
	@Path("/profile/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserProfile(@PathParam("email") String email) {
		System.out.println("User Details");
		UsersDAO udao = new UsersDAO();
		// Users users = udao.getUserProfile(email);
		// return users;
		return Response.ok(udao.getUserProfile(email), MediaType.APPLICATION_JSON).build();

	}

	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String Login(Users usr) {
		UsersDAO dao = new UsersDAO();
		String str = "";
		boolean response = dao.getUsersbyEmailId(usr);

		if (response == true) {

			str = "Login Successful";
			return str;
		}

		else {
			str = "Invalid Email or Passwords";
			return str;

		}

	}

	@PUT
	@Path("/modify/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUsers(@PathParam("id") String id, Users usr) {
		UsersDAO udao = new UsersDAO();
		udao.updateUsers(id, usr);
		return Response.ok(udao.updateUsers(id, usr), MediaType.APPLICATION_JSON).build();
	}

	@DELETE
	@Path("/delete/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteFlightBookings(@PathParam("id") String id) {
		UsersDAO udao = new UsersDAO();
		int count = udao.deleteUsers(id);
		if (count == 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		return Response.ok().build();
	}

}
