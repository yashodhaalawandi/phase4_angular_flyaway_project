package com.FSD.Phase2.Project.FlyAwayProject1;

import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
//import javax.ws.rs.core.MultivaluedMap;
 
@Provider
  public class CorsResponseFilter implements ContainerResponseFilter {
  
	@Override
  public void filter(ContainerRequestContext requestContext,
  ContainerResponseContext responseContext) throws IOException {
  
  //MultivaluedMap<String, Object> headers = responseContext.getHeaders();
  
  responseContext.getHeaders().add("Access-Control-Allow-Origin","http://localhost:4200");
 
  
  responseContext.getHeaders().add("Access-Control-Allow-Methods", "GET,POST,PATCH,DELETE,PUT,OPTIONS,HEAD");
  //responseContext.getHeaders().add("Access-Control-Allow-Headers","X-Requested-With, Content-Type");
  responseContext.getHeaders().add("Access-Control-Allow-Headers","*");
  
  responseContext.getHeaders().add("Access-Control-Allow-Headers","accept,origin,ontent-type,append,delete,entries,foreach,get,has,keys,set,values,Authorization");
  
 // responseContext.getHeaders().add("Access-Control-Allow-Headers","origin, content-type, accept, authorization");
  //append,delete,entries,foreach,get,has,keys,set,values,Authorization
  responseContext.getHeaders().add("Access-Control-Allow-Credentials", "true");
  
  
  
  }
  
  }
 



