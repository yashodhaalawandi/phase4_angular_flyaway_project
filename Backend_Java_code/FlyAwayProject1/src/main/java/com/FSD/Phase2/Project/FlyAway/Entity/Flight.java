package com.FSD.Phase2.Project.FlyAway.Entity;


import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name="Flight_New")
public class Flight {
	
	@Id
	@Column (name="flight_number")
	private String number;
	
	@Column (name="Departure_Time")
	private String departure;
	
	@Column (name="Arrival_Time")
	private String arrival;
		
	@Column (name="Flight_Cost")
	private String flightcost;
	

	@Column (name="From_City")
	private String from_city;
	
	@Column (name="To_City")
	private String to_city;
	
	
	@ManyToMany(mappedBy = "flights", fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<FlightBookings> bookings;


	public Flight() {
		super();
		
	}


	public Flight(String number, String departure, String arrival, String from_city,
			String to_city, String flightcost) {
		this.number = number;
		this.departure = departure;
		this.arrival = arrival;
		this.from_city = from_city;
		this.to_city = to_city;
		this.flightcost = flightcost;
		}





	public String getNumber() {
		return number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	public String getDeparture() {
		return departure;
	}


	public void setDeparture(String departure) {
		this.departure = departure;
	}


	public String getArrival() {
		return arrival;
	}


	public void setArrival(String arrival) {
		this.arrival = arrival;
	}


	


	public String getFrom_city() {
		return from_city;
	}


	public void setFrom_city(String from_city) {
		this.from_city = from_city;
	}


	public String getTo_city() {
		return to_city;
	}


	public void setTo_city(String to_city) {
		this.to_city = to_city;
	}


	public String getFlightcost() {
		return flightcost;
	}


	public void setFlightcost(String flightcost) {
		this.flightcost = flightcost;
	}


	public Set<FlightBookings> getBookings() {
		return bookings;
	}


	public void setBookings(Set<FlightBookings> bookings) {
		this.bookings = bookings;
	}


	@Override
	public String toString() {
		return "Flight [number=" + number + ", departure=" + departure + ", arrival=" + arrival + ", departureDate="
				+ from_city + ", arrivalDate=" + to_city + ", flightcost=" + flightcost + "]";
	}
	
	
	
	
	

}
