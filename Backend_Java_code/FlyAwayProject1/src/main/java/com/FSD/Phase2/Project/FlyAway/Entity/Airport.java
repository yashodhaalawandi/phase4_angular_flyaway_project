package com.FSD.Phase2.Project.FlyAway.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Airport")
public class Airport {
	
	@Id
	@Column(name="airport_code")
	private String airportcode;
	
	@Column(name="airport_name")
	private String airportname;
	
	@Column(name="location")
	private String location;
	

	public Airport() {
		super();
		
	}

	public Airport(String airportcode, String airportname, String location) {
		
		this.airportcode = airportcode;
		this.airportname = airportname;
		this.location=location;
	}

	public String getAirportcode() {
		return airportcode;
	}

	public void setAirportcode(String airportcode) {
		this.airportcode = airportcode;
	}

	public String getAirportname() {
		return airportname;
	}

	public void setAirportname(String airportname) {
		this.airportname = airportname;
	}
	

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Airport [airportcode=" + airportcode + ", airportname=" + airportname + ", location=" + location + "]";
	}

	
	
	

}
