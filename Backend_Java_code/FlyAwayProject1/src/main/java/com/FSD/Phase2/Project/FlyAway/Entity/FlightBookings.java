package com.FSD.Phase2.Project.FlyAway.Entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.glassfish.jersey.Beta;

import com.fasterxml.jackson.annotation.JsonBackReference;



@Entity
@Table(name="Flight_Bookings")
public class FlightBookings {
	
	@Id
	@Column(name="booking_number")
	private String number;
	
	
	@Column(name="Customer_Number")
	private String customernumber;

	
	@ManyToOne
	private Passenger passenger;
	
	@ManyToMany(cascade = { CascadeType.ALL },fetch = FetchType.EAGER)
	@JoinTable(name = "Flight_book_Join", joinColumns = @JoinColumn(name ="booking_number"),inverseJoinColumns = @JoinColumn(name = "flight_number"))
	@JsonBackReference
	private Set<Flight> flights;

	public FlightBookings() {
		super();
		
	}

	

	public FlightBookings(String number, String customernumber, Passenger passenger, Set<Flight> flights) {
		this.number = number;
		this.customernumber = customernumber;
		this.passenger = passenger;
		this.flights = flights;
	}



	

	public String getNumber() {
		return number;
	}



	public void setNumber(String number) {
		this.number = number;
	}



	public String getCustomernumber() {
		return customernumber;
	}



	public void setCustomernumber(String customernumber) {
		this.customernumber = customernumber;
	}



	public Passenger getPassenger() {
		return passenger;
	}



	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}



	public Set<Flight> getFlights() {
		return flights;
	}



	public void setFlights(Set<Flight> flights) {
		this.flights = flights;
	}



	@Override
	public String toString() {
		return "FlightBookings [number=" + number + ", customernumber=" + customernumber + "]";
	}
	
	
	
	
	

}
