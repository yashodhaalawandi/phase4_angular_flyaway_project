import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { AuthModule } from './Auth/auth.module';
import { FormsModule } from '@angular/forms';


const routes: Routes = [
  {path:"", component:HomepageComponent},
  {path:"home",component:HomepageComponent},
  {path:"aboutus", component:AboutusComponent},
  {path:"auth", loadChildren:()=>import('./auth/auth.module').then(m=>m.AuthModule)}
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes),AuthModule,FormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
