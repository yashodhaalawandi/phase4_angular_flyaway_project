import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './Register/register.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

const routes:Routes=[
  {path:"login", component:LoginComponent},
{path:"register",component:RegisterComponent}]

@NgModule({
  declarations: [
    LoginComponent,RegisterComponent
  ],
  imports: [
    CommonModule, RouterModule.forChild(routes),FormsModule,AuthModule
  ],
  exports: [RouterModule]
})
export class AuthModule { }
  